import React, {Component} from 'react';
import box from './box.png';
import './App.css';
import soundFile from './click.wav';

const sound = new Audio(soundFile);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      lastOk: true,
    };
  }

  async sendMessage(e) {
    const { status } = this.state;
    if (status !== '') {
      return;
    }
    e.preventDefault();
    sound.play();

    try {
      const response = await fetch('/playRandom', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({value: true}),
      });
      const content = await response.text();
      this.setState({
        lastOk: response.ok,
        status: content,
      });
      if (response.ok) {
        setTimeout(() => {
          this.setState({status: ''});
        }, 2700)
      }
    } catch (e) {
      console.log(e);
      this.setState({lastOk: false});
    }
  }

  loading() {
    return (
      <div className="App">
        <header className="App-header">
          <p>Loading…</p>
        </header>
      </div>
    );
  }

  renderOffline() {
    return (
      <div className="App">
        <header className="App-header">
          <p className="error">
            Offline{' '}
            <span role="img" aria-label="sad face">
              😢
            </span>
            . Tell Eric.
          </p>
        </header>
      </div>
    );
  }
  render() {
    const {lastOk, status} = this.state;
    return (
      <div className="App">
        <header className="App-header">
          {lastOk && (
            <p>{status}</p>
          )}
          {!lastOk && (
            <p className="error">
              Error playing clip{' '}
              <span role="img" aria-label="sad face">
                😢
              </span>
              : {status}.
            </p>
          )}
          <div>
            <img
              src={box}
              className={status !== '' ? 'blackandwhite' : null}
              alt="logo"
              onClick={this.sendMessage.bind(this)}
            />
          </div>
        </header>
      </div>
    );
  }
}
export default App;
