const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

exports.handler = async function(event, context) {
  try {
    return {
      statusCode: 200,
      body: JSON.stringify({
      }),
    };
  } catch (e) {
    console.log(e);
  }
  return failure;
};
