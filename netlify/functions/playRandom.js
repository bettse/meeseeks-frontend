const fetch = require('node-fetch');
const cookie = require('cookie')
const { verify } = require('jsonwebtoken');

const { MEESEEKS_PATH, JWT_SECRET } = process.env

const PLAY_LIMIT_MS = 5 * 60 * 1000;

const failure = {
  statusCode: 503,
  body: JSON.stringify({}),
};

const url = `https://sockethook.ericbetts.dev/hook/${MEESEEKS_PATH}`;

let lastPlay = 0;

async function checkAuth(headers) {
  if (!headers.cookie) {
    return false;
  }

  const cookies = cookie.parse(headers.cookie);
  if (!cookies || !cookies.nf_jwt) {
    return false
  }
  const { nf_jwt } = cookies;
  try {
    const decoded = await verify(nf_jwt, JWT_SECRET);
    const { app_metadata } = decoded;
    const { authorization } = app_metadata;
    const { roles} = authorization
    return roles.includes('user');
  } catch (e) {
    return false;
  }
}

exports.handler = async function(event, context) {
  const { headers } = event;
  const isUser = await checkAuth(headers);
  const now = new Date().getTime();
  try {
    // registered users may bypass the time limit
    if (isUser || (now - lastPlay > PLAY_LIMIT_MS)) {
      if (!isUser) { // Don't count registered users towards timeout
        lastPlay = now;
      }
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify({value: true}),
      });
      const body = await response.text();
      return {
        statusCode: response.ok ? 200 : 503,
        body,
      };
    } else {
      return {
        statusCode: 404,
        body: 'please wait',
      };
    }
  } catch (e) {
    console.log(e);
  }
  return failure;
};
